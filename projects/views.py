from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    return render(request, "projects/list.html", {"projects": projects})


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    return render(request, "projects/detail.html", {"project": project})


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            projects = form.save(False)
            projects.owner = request.user
            projects.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    return render(request, "projects/create.html", {"form": form})
